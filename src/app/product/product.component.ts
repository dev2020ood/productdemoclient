import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {map, debounceTime} from 'rxjs/operators'
import { ProductService } from '../product.service';
import { FormGroup, FormControl, Validators, AbstractControl, ValidationErrors } from '@angular/forms';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  saveProduct$:Observable<any>
  constructor(private productService:ProductService) { }
  form:FormGroup
  ngOnInit(): void {
    this.form = new FormGroup({
      ID:new FormControl('',[Validators.required],[this.productService.CheckIDDebounce(1000)]),
      Name:new FormControl('',[Validators.required,Validators.email])
    })
    console.log(this.form)
    /*
    this.saveProduct$ = this.productService.SaveProduct()
    console.log("Init SaveProduct$")
    */
    this.productService.OnSaveProductOK().subscribe(response=>console.log("OK"))
    this.productService.OnInvalidProductID().subscribe(response=>console.log("InvalidID"))
    this.productService.OnInvalidProductName().subscribe(response=>console.log("InvalidName"))


  }
  SaveData(){
    
    
    this.productService.SaveProduct(this.form.value)
    /*
    var IDS$ = this.saveProduct$.pipe(
      map(response=>response.id)
    )
    this.saveProduct$
    .subscribe(resp=>console.log(resp),
    error=>console.log(error),
    ()=>console.log("Completed"))
   
     IDS$.subscribe(id=>console.log("ID is ==>",id)) 
     */

  }
  get ID(){
    return this.form.get("ID")
  }
  get Name(){
    return this.form.get("Name")
  }
  getIDErrors(){
    return JSON.stringify(this.ID.errors)
  }
  getNameErrors(){
    return JSON.stringify(this.Name.errors)
  }
 
  

}
