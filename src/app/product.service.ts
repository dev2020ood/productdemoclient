import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable, Subject } from 'rxjs';
import { AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { debounce, debounceTime, map, switchMap, take, tap } from 'rxjs/operators';
import { CommService } from './comm.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  isNameExists(value: any): Observable<boolean> {
    var subj  = new Subject<boolean>()
    return subj
  }

  ResponseSubjects={
    "SaveProductOK":new Subject<any>(),
    "SaveProductInvalidID":new Subject<any>(),
    "SaveProductInvalidName":new Subject<any>()

      
    }

  
  constructor(private http:HttpClient,private commService:CommService) { 

  }
  SaveProduct(value:any){
    var url = "api/products/SaveProduct"
    
    var saveData$ = this.http.post(url,value)
    saveData$.subscribe(response=>this.ResponseSubjects[response["responseType"]].next(response))
    
      
  }
  OnSaveProductOK():Observable<any>
  {
      return this.ResponseSubjects["SaveProductOK"]
  }

  OnInvalidProductID():Observable<any>{
    return this.ResponseSubjects["SaveProductInvalidID"]
  }

  OnInvalidProductName():Observable<any>{
    return this.ResponseSubjects["SaveProductInvalidName"]
  }
  CheckID():AsyncValidatorFn
    {
      var url = "api/products/ValidateID"
      return (control:AbstractControl):
            Observable<ValidationErrors|null> =>{
                var obs$  =  
                 this.http.post(url,{ID:control.value,Name:""}).
                pipe(
                 
                  map((response:any)=>response.responseType=="ResponseIDOK"?null
                  :{"productIDExists":"Invalid"})
                )
               
                return obs$


            } 
      
    }
  CheckIDDebounce(delayMs):AsyncValidatorFn{
    var url = "api/products/ValidateID"
    return (control:AbstractControl):
    Observable<ValidationErrors|null> =>
    {
         
        var comm$  =  
         this.http.post(url,{ID:control.value,Name:""}).
        pipe(
         
          map((response:any)=>response.responseType=="ResponseIDOK"?null
          :{"productIDExists":"Invalid"})
        )
        var obs$ = control.valueChanges.pipe(
          debounceTime(delayMs),
          switchMap(value=>comm$),
          take(1)
          
        )
        
        return obs$


    } 

  }


}
